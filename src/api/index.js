import instance from "../utils/Service";

// Alter defaults after instance has been created
// instance.defaults.headers.common['Authorization'] = AUTH_TOKEN;

const Api = {
  //user
  login: value => {
    const url = "/Users/login";
    return instance.post(url, value);
  },
  //company
  fetchcompany: () => {
    const url = "/Companies";
    return instance.get(url);
  },
  createcompany: value => {
    const url = "/Companies";
    return instance.post(url, value.data);
  },
  updatecompany: value => {
    const url = "/Companies";
    return instance.patch(url, value.data);
  },
  deletecompany: value => {
    console.log("API => deletecompany()  recieved value is ", value);
    const url = `/Companies/${value.data.id}`;
    return instance.delete(url);
  },

  // Distribution
  fetchdistribution: () => {
    const url = "/Distributions";
    return instance.get(url);
  },
  createdistribution: value => {
    const url = "/Distributions";
    return instance.post(url, value.data);
  },
  updatedistribution: value => {
    const url = "/Distributions";
    return instance.patch(url, value.data);
  },
  deletedistribution: value => {
    console.log("API => deletedistribution()  recieved value is ", value);
    const url = `/Distributions/${value.data.id}`;
    return instance.delete(url);
  },

  // SubDistribution
  fetchsubdistribution: () => {
    const url = "/Subdistributions";
    return instance.get(url);
  },
  createsubdistribution: value => {
    const url = "/Subdistributions";
    return instance.post(url, value.data);
  },
  updatedsubistribution: value => {
    const url = "/Subdistributions";
    return instance.patch(url, value.data);
  },
  deletesubdistribution: value => {
    console.log("API => deletedsubdistribution()  recieved value is ", value);
    const url = `/Subdistributions/${value.data.id}`;
    return instance.delete(url);
  },

  //calculation
  fetchcalculation: () => {
    const url = "/Calculations";
    return instance.get(url);
  },
  createcalculation: value => {
    const url = "/Calculations";
    return instance.post(url, value.data);
  },
  updatecalculation: value => {
    const url = "/Calculations";
    return instance.patch(url, value.data);
  },
  deletecalculation: value => {
    console.log("API => deletecalculation()  recieved value is ", value);
    const url = `/Calculations/${value.data.id}`;
    return instance.delete(url);
  },

  //collection
  fetchcollection: () => {
    const url = "/Collections";
    return instance.get(url);
  },
  createcollection: value => {
    const url = "/Collections";
    return instance.post(url, value.data);
  },
  updatecollection: value => {
    const url = "/Collections";
    return instance.patch(url, value.data);
  },
  deletecollection: value => {
    console.log("API => deletecollection()  recieved value is ", value);
    const url = `/Collections/${value.data.id}`;
    return instance.delete(url);
  }
};

export default Api;
