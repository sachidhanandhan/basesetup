import instance from "../utils/Service";

// Alter defaults after instance has been created
// instance.defaults.headers.common['Authorization'] = AUTH_TOKEN;

const Api = {
  //company
  fetchcollection: value => {
    const url = `/Collections?filter={"where": {"movie_id": ${
      value.data.movieId
    }}}`;
    return instance.get(url);
  },
  fetchtheatrebymovie: value => {
    const url = `/Theaters/for-collection?movie_id=${
      value.data.movieId
    }&company_id=${value.data.companyId}&date=${value.data.date}`;

    return instance.get(url);
  },
  createCollection: value => {
    const url = "/Collections/enter";
    return instance.post(url, value.data); //post for mokeable purpose change to patch
  }
  //   deletecompany: value => {
  //     console.log("API => deletecompany()  recieved value is ", value);
  //     // const url = `/Companies/${value.data.company.id}`;
  //     const url = `/Companies`;
  //     return mockable_instance.delete(url);
  //   }
};

export default Api;
