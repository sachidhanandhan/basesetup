import instance from "../utils/Service";
const Api = {
  //agreement
  fetchagreement: () => {
    const url = "/TheaterAgreements";
    return instance.get(url);
  },
  createagreement: value => {
    const url = "/TheaterAgreements";
    return instance.post(url, value.data);
  },
  updateagreement: value => {
    const url = "/TheaterAgreements";
    return instance.patch(url, value.data);
  },
  deleteagreement: value => {
    console.log("API => deleteagreement()  recieved value is ", value);
    const url = `/TheaterAgreements/${value.data.id}`;
    return instance.delete(url);
  }
};
export default Api;
