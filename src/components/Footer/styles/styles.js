const styles = theme => ({
  footer: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing.unit * 8,
    position: "absolute",
    width: "100%",
    bottom: 0
  }
});

export default styles;
