import React from "react";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import RadioGroup from "@material-ui/core/RadioGroup";
import Checkbox from "@material-ui/core/Checkbox";
import { FormControl, InputLabel } from "@material-ui/core";
// // import Select  from '@material-ui/core/Select'
import Select from "react-select";
import CreatableSelect from "react-select/lib/Creatable";
import "./styles/index.css";
import { Switch } from "@material-ui/core";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const renderTextField = ({
  input,
  label,
  meta: { touched, error, warning },
  ...custom
}) => (
    <>

      <TextField
        hintText={label}
        label={label}
        error={touched && error}
        {...input}
        {...custom}
      />
      <div>
        {touched &&
          ((error && <span className="input-error">{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </>
  );

const renderTextAreaField = ({
  input,
  label,
  multiline,
  rowsMax,
  meta: { touched, error },
  ...custom
}) => (
    <TextField
      hintText={label}
      label={label}
      multiline
      rowsMax={rowsMax}
      errorText={touched && error}
      {...input}
      {...custom}
    />
  );

const renderSwitch = ({ input, label, name, value }) => (
  <Switch label={label} name={name} checked={value} onCheck={input.onChange} />
);

const renderCheckbox = ({ input, label }) => (
  <Checkbox
    label={label}
    checked={input.value ? true : false}
    onCheck={input.onChange}
  />
);
class CheckBoxInput extends React.Component {
  onCheckBoxSelectChange = input => {
    input.onChange();
  };

  render() {
    const { label, input } = this.props;
    let name = input.name;

    return (
      <div>
        <InputLabel htmlFor={label} style={{ paddingTop: "15px" }}>
          {" "}
          {label}{" "}
        </InputLabel>
        <FormControl {...input}>
          <Checkbox
            name={name}
            label={label}
            color="primary"
            checked={input.value ? true : false}
            onChange={() => this.onCheckBoxSelectChange(input)}
          />
        </FormControl>
      </div>
    );
  }
}

const renderRadioGroup = ({ input, ...rest }) => (
  <RadioGroup
    {...input}
    {...rest}
    valueSelected={input.value}
    onChange={(event, value) => input.onChange(value)}
  />
);

const renderSelectField = ({
  input,
  label,
  meta: { touched, error, warning },
  className,
  classNamePrefix = "",
  name,
  options = [],
  isDisabled = false,
  isLoading = false,
  isClearable = true,
  isSearchable = true,
  isMulti = false,
  getOptionLabel,
  getOptionValue,
  ...custom
}) => (
    <>
      <label className="input-label">
        {label}
      </label>
      <Select
        placeholder={label}
        value={input.value}
        className={className}
        classNamePrefix={classNamePrefix}
        isDisabled={isDisabled}
        isLoading={isLoading}
        isClearable={isClearable}
        isSearchable={isSearchable}
        name={name}
        onChange={value => input.onChange(value)}
        onBlur={() => input.onBlur(input.value)}
        isMulti={isMulti}
        options={options}
        getOptionLabel={getOptionLabel}
        getOptionValue={getOptionValue}
        {...custom}
      />
      <div>
        {touched &&
          ((error && <span className="input-error">{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </>
  );

const renderCreatableSelect = ({
  input,
  label,
  meta: { touched, error, warning },
  className,
  classNamePrefix = "",
  name,
  defaultValue = [],
  options = [],
  isDisabled = false,
  isLoading = false,
  isClearable = true,
  isSearchable = true,
  isMulti = false,
  ...custom
}) => (
    <>
      <label className="input-label">
        {label}
      </label>
      <CreatableSelect
        placeholder={label}
        value={input.value}
        className={className}
        classNamePrefix={classNamePrefix}
        defaultValue={defaultValue}
        isDisabled={isDisabled}
        isLoading={isLoading}
        isClearable={isClearable}
        isSearchable={isSearchable}
        name={name}
        onChange={value => input.onChange(value)}
        onBlur={() => input.onBlur(input.value)}
        isMulti={true}
        options={[]}
        {...custom}
      />
      <div>
        {touched &&
          ((error && <span className="input-error">{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </>
  );
const DateTimePickerRow = ({
  input,
  label,
  meta: { touched, error, warning },
  className,
  classNamePrefix = "",
  timeCaption = "",
  name,
  showTimeSelect,
  showTimeSelectOnly,
  timeIntervals = 15,
  dateFormat = "",
  ...custom
}) => (
    <>
      <label className="input-label">
        {label}
      </label>
      <DatePicker
        className="date-time-picker"
        selected={input.value}
        onChange={value => input.onChange(value)}
        showTimeSelect={showTimeSelect}
        showTimeSelectOnly={showTimeSelectOnly}
        dateFormat={dateFormat}
        timeCaption={timeCaption}
      />
      <div>
        {touched &&
          ((error && <span className="input-error">{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </>
  );
export {
  renderTextField,
  renderSelectField,
  renderSwitch,
  renderCheckbox,
  CheckBoxInput,
  renderCreatableSelect,
  DateTimePickerRow
};
