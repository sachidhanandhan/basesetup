import React from "react";
import { Switch, Route } from "react-router-dom";

import PrivateRoute from "./PrivateRouter";

const Router = ({ routes }) => {
  return (
    <Switch>
      {routes.map(
        ({ isPrivate = false, breadcrumb, exact = true, ...route }, key) => {
          const RouteComponent = isPrivate ? PrivateRoute : Route;
          return <RouteComponent exact={exact} key={key} {...route} />;
        },
      )}
    </Switch>
  );
};

export default Router;
