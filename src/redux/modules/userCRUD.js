import {
  FETCH_USER_SUCCEEDED,
  ADD_NEW_USER_SUCCEEDED,
  EDIT_USER,
  UPDATE_USER_SUCCEEDED,
  CLEAR_USER,
  DELETE_USER_SUCCEEDED
} from "../../actions/actionConstants";

const initialState = {
  userList: [],
  formValues: [],
  userEdit: {},
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    // -------------  User -------------------------------

    case FETCH_USER_SUCCEEDED:
      return {
        ...state,
        userList: action.data
      };
    case ADD_NEW_USER_SUCCEEDED:
      const newuserList = [...state.userList];
      newuserList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        userList: newuserList
      };
    case UPDATE_USER_SUCCEEDED:
      const objIndex = state.userList.findIndex(
        obj => obj.id === action.data.data.id
      );

      // make new object of updated object.
      const updatedObj = { ...action.data.data };

      // make final new array of objects by combining updated object.
      const updateduserList = [
        ...state.userList.slice(0, objIndex),
        updatedObj,
        ...state.userList.slice(objIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        userList: updateduserList
      };
    case EDIT_USER:
      let user = { ...action.data.user };
      user.company = {
        ...action.data.company,
      };
      user.role = {
        label: action.data.user.roles[0].name,
        value: action.data.user.roles[0].id
      };

      return {
        ...state,
        userEdit: user
      };
    case CLEAR_USER:
      return {
        ...state,
        userEdit: {}
      };
    case DELETE_USER_SUCCEEDED:
      const delIndex = state.userList.findIndex(
        obj => obj.id === action.data.data.id
      );

      const afterDeleteuserList = [
        ...state.userList.slice(0, delIndex),
        ...state.userList.slice(delIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        userList: afterDeleteuserList
      };

    default:
      return state;
  }
}
