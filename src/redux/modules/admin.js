import {
  DELETE_SUBDISTRIBUTION_SUCCEEDED,
  CLEAR_SUBDISTRIBUTION,
  EDIT_SUBDISTRIBUTION,
  UPDATE_SUBDISTRIBUTION_SUCCEEDED,
  ADD_NEW_SUBDISTRIBUTION_SUCCEEDED,
  FETCH_SUBDISTRIBUTION_SUCCEEDED,
  DELETE_DISTRIBUTION_SUCCEEDED,
  CLEAR_DISTRIBUTION,
  EDIT_DISTRIBUTION,
  UPDATE_DISTRIBUTION_SUCCEEDED,
  ADD_NEW_DISTRIBUTION_SUCCEEDED,
  FETCH_DISTRIBUTION_SUCCEEDED,
  DELETE_CALCULATION_SUCCEEDED,
  CLEAR_CALCULATION,
  EDIT_CALCULATION,
  UPDATE_CALCULATION_SUCCEEDED,
  ADD_NEW_CALCULATION_SUCCEEDED,
  FETCH_CALCULATION_SUCCEEDED
} from "../../actions/actionConstants";

const initialState = {
  theatreList: [],
  formValues: [],
  distributionLocationList: [],
  subDistributionLocationList: [],
  distributionList: [],
  subDistributionList: [],
  companyEdit: {},
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    // -------------  Calculation -------------------------------

    case FETCH_CALCULATION_SUCCEEDED:
      return {
        ...state,
        calculationList: action.data
      };
    case ADD_NEW_CALCULATION_SUCCEEDED:
      const calculationList = state.calculationList;
      calculationList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        calculationList
      };
    case UPDATE_CALCULATION_SUCCEEDED:
      const calculationobjIndex = state.calculationList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const calculationupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const updatedcalculationList = [
        ...state.calculationList.slice(0, calculationobjIndex),
        calculationupdatedObj,
        ...state.calculationList.slice(calculationobjIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        calculationList: updatedcalculationList
      };
    case EDIT_CALCULATION:
      console.log("----------EDIT_CALCULATION------------");
      console.log(action.data);
      console.log("----------------------");
      return {
        ...state,
        calculationEdit: action.data
      };
    case CLEAR_CALCULATION:
      return {
        ...state,
        calculationEdit: {}
      };
    case DELETE_CALCULATION_SUCCEEDED:
      const calculationindex = state.calculationList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const calculationrecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const afterDeletecalculationList = [
        ...state.calculationList.slice(0, calculationindex),
        calculationrecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        calculationList: afterDeletecalculationList
      };

    // ------------- DISTRIBUTION  -------------------------------

    case FETCH_DISTRIBUTION_SUCCEEDED:
      return {
        ...state,
        distributionList: action.data
      };
    case ADD_NEW_DISTRIBUTION_SUCCEEDED:
      const distributionList = state.distributionList;
      distributionList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        distributionList
      };
    case UPDATE_DISTRIBUTION_SUCCEEDED:
      const distributionListobjIndex = state.distributionList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const distributionListupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const distributionListupdated = [
        ...state.distributionList.slice(0, distributionListobjIndex),
        distributionListupdatedObj,
        ...state.distributionList.slice(distributionListobjIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        distributionList: distributionListupdated
      };
    case EDIT_DISTRIBUTION:
      console.log("----------EDIT_DISTRIBUTION------------");
      console.log(action.data);
      console.log("----------------------");
      return {
        ...state,
        distributionEdit: action.data
      };
    case CLEAR_DISTRIBUTION:
      return {
        ...state,
        distributionEdit: {}
      };
    case DELETE_DISTRIBUTION_SUCCEEDED:
      const distributionListindex = state.distributionList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const distributionListrecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const afterDeletedistributionList = [
        ...state.distributionList.slice(0, distributionListindex),
        distributionListrecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        distributionList: afterDeletedistributionList
      };

    // ------------------ SUBDISTRIBUTION -------------------------

    case FETCH_SUBDISTRIBUTION_SUCCEEDED:
      return {
        ...state,
        subDistributionList: action.data
      };
    case ADD_NEW_SUBDISTRIBUTION_SUCCEEDED:
      const subDistributionList = state.subDistributionList;
      subDistributionList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        subDistributionList
      };
    case UPDATE_SUBDISTRIBUTION_SUCCEEDED:
      const subDistributionListobjIndex = state.subDistributionList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const subDistributionListupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const subDistributionListupdated = [
        ...state.subDistributionList.slice(0, subDistributionListobjIndex),
        subDistributionListupdatedObj,
        ...state.subDistributionList.slice(subDistributionListobjIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        subDistributionList: subDistributionListupdated
      };
    case EDIT_SUBDISTRIBUTION:
      console.log("----------EDIT SUBDISTRIBUTION------------");
      console.log(action.data);
      console.log("----------------------");
      return {
        ...state,
        subDistributionEdit: action.data
      };
    case CLEAR_SUBDISTRIBUTION:
      return {
        ...state,
        subDistributionEdit: {}
      };
    case DELETE_SUBDISTRIBUTION_SUCCEEDED:
      const subDistributionListindex = state.subDistributionList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const subDistributionListrecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const afterDeletesubDistributionList = [
        ...state.subDistributionList.slice(0, subDistributionListindex),
        subDistributionListrecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        subDistributionList: afterDeletesubDistributionList
      };
    default:
      return state;
  }
}
