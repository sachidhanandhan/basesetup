import {
  LOGIN_SUCCEEDED,
  LOGOUT_SUCCEEDED
} from "../../actions/actionConstants";
import Auth from "../../components/Auth";
const initialState = {
  user: {},
  isLogIn: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN_SUCCEEDED:
      console.log("Redux -> Login -> LOGIN_SUCCEEEDED Case", action);
      Auth.setSessionStorage(action.data.id);
      return {
        ...state,
        user: action.data,
        isLogIn: true
      };
    case LOGOUT_SUCCEEDED:
      console.log("====================================");
      console.log("Redux => logout Reducer");
      console.log("====================================");
      return {
        ...state,
        isLogIn: false
      };

    default:
      return state;
  }
}
