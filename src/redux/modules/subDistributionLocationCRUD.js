import {
  FETCH_SUBDISTRIBUTIONLOCATION_SUCCEEDED,
  ADD_NEW_SUBDISTRIBUTIONLOCATION_SUCCEEDED,
  UPDATE_SUBDISTRIBUTIONLOCATION_SUCCEEDED,
  EDIT_SUBDISTRIBUTIONLOCATION,
  CLEAR_SUBDISTRIBUTIONLOCATION,
  DELETE_SUBDISTRIBUTIONLOCATION_SUCCEEDED
} from "../../actions/actionConstants";

const initialState = {
  formValues: [],
  subDistributionLocationList: [],
  subDistributionLocationEdit: {},
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    // ------------------ SUBDISTRIBUTION LOCAITON -------------------------

    case FETCH_SUBDISTRIBUTIONLOCATION_SUCCEEDED:
      return {
        ...state,
        subDistributionLocationList: action.data
      };
    case ADD_NEW_SUBDISTRIBUTIONLOCATION_SUCCEEDED:
      const subDistributionLocationList = state.subDistributionLocationList;
      subDistributionLocationList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        subDistributionLocationList
      };
    case UPDATE_SUBDISTRIBUTIONLOCATION_SUCCEEDED:
      const subDistributionLocationListobjIndex = state.subDistributionLocationList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const subDistributionLocationListupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const subDistributionLocationListupdated = [
        ...state.subDistributionLocationList.slice(
          0,
          subDistributionLocationListobjIndex
        ),
        subDistributionLocationListupdatedObj,
        ...state.subDistributionLocationList.slice(
          subDistributionLocationListobjIndex + 1
        )
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        subDistributionLocationList: subDistributionLocationListupdated
      };
    case EDIT_SUBDISTRIBUTIONLOCATION:
      console.log("----------EDIT_SUBDISTRIBUTIONLOCATION------------");
      console.log(action.data);
      console.log("----------------------");
      return {
        ...state,
        subDistributionLocationEdit: action.data
      };
    case CLEAR_SUBDISTRIBUTIONLOCATION:
      return {
        ...state,
        subDistributionLocationEdit: {}
      };
    case DELETE_SUBDISTRIBUTIONLOCATION_SUCCEEDED:
      const subDistributionLocationListindex = state.subDistributionLocationList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const subDistributionLocationListrecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const afterDeletesubDistributionLocationList = [
        ...state.subDistributionLocationList.slice(
          0,
          subDistributionLocationListindex
        ),
        subDistributionLocationListrecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        subDistributionLocationList: afterDeletesubDistributionLocationList
      };
    default:
      return state;
  }
}
