import {
  FETCH_COMPANY_SUCCEEDED,
  FETCH_COMPANY_FORM_SUCCEEDED,
  ADD_NEW_COMPANY_SUCCEEDED,
  EDIT_COMPANY,
  UPDATE_COMPANY_SUCCEEDED,
  CLEAR_COMPANY,
  DELETE_COMPANY_SUCCEEDED
} from "../../actions/actionConstants";

const initialState = {
  companyList: [],
  companyFormList: [],
  formValues: [],
  companyEdit: {},
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    // -------------  Company -------------------------------

    case FETCH_COMPANY_SUCCEEDED:
      return {
        ...state,
        companyList: action.data
      };
    case FETCH_COMPANY_FORM_SUCCEEDED:
      return {
        ...state,
        companyFormList: action.data
      };
    case ADD_NEW_COMPANY_SUCCEEDED:
      const newCompanyList = [...state.companyList];
      newCompanyList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        companyList: newCompanyList
      };
    case UPDATE_COMPANY_SUCCEEDED:
      const objIndex = state.companyList.findIndex(
        obj => obj.company.id === action.data.data.company.id
      );

      // make new object of updated object.
      const updatedObj = { ...action.data.data };

      // make final new array of objects by combining updated object.
      const updatedCompanyList = [
        ...state.companyList.slice(0, objIndex),
        updatedObj,
        ...state.companyList.slice(objIndex + 1)
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        companyList: updatedCompanyList
      };
    case EDIT_COMPANY:
      console.log(
        "EDIT_COMPANY action.data----------------------------------------"
      );
      console.log(action.data);

      return {
        ...state,
        companyEdit: action.data
      };
    case CLEAR_COMPANY:
      console.log(
        "CLEAR_COMPANY---------------------------------------------------"
      );

      return {
        ...state,
        companyEdit: {}
      };
    case DELETE_COMPANY_SUCCEEDED:
      const delIndex = state.companyList.findIndex(
        obj => obj.company.id === action.data.data.company.id
      );

      console.log(delIndex);
      // make final new array of objects by combining updated object.
      const afterDeleteCompanyList = [
        ...state.companyList.slice(0, delIndex),
        ...state.companyList.slice(delIndex + 1)
      ];
      console.log(afterDeleteCompanyList);
      return {
        ...state,
        isSubmitSuccess: true,
        companyList: afterDeleteCompanyList
      };

    default:
      return state;
  }
}
