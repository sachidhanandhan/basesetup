import {
  FETCH_DISTRIBUTIONLOCATION_SUCCEEDED,
  ADD_NEW_DISTRIBUTIONLOCATION_SUCCEEDED,
  UPDATE_DISTRIBUTIONLOCATION_SUCCEEDED,
  EDIT_DISTRIBUTIONLOCATION,
  CLEAR_DISTRIBUTIONLOCATION,
  DELETE_DISTRIBUTIONLOCATION_SUCCEEDED
} from "../../actions/actionConstants";
const initialState = {
  formValues: [],
  distributionLocationList: [],
  distributionLocationEdit: {},
  editingId: "",
  isEdit: false,
  isSubmitSuccess: false,
  showFrm: false,
  notifMsg: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    // ------------- DISTRIBUTION LOCAITON -------------------------------

    case FETCH_DISTRIBUTIONLOCATION_SUCCEEDED:
      return {
        ...state,
        distributionLocationList: action.data
      };
    case ADD_NEW_DISTRIBUTIONLOCATION_SUCCEEDED:
      const distributionLocationList = state.distributionLocationList;
      distributionLocationList.push(action.data.data);
      return {
        ...state,
        isSubmitSuccess: true,
        distributionLocationList
      };
    case UPDATE_DISTRIBUTIONLOCATION_SUCCEEDED:
      const distributionLocationListobjIndex = state.distributionLocationList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const distributionLocationListupdatedObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const distributionLocationListupdated = [
        ...state.distributionLocationList.slice(
          0,
          distributionLocationListobjIndex
        ),
        distributionLocationListupdatedObj,
        ...state.distributionLocationList.slice(
          distributionLocationListobjIndex + 1
        )
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        distributionLocationList: distributionLocationListupdated
      };
    case EDIT_DISTRIBUTIONLOCATION:
      console.log("----------EDIT_DISTRIBUTIONLOCATION------------");
      console.log(action.data);
      console.log("----------------------");
      return {
        ...state,
        distributionLocationEdit: action.data
      };
    case CLEAR_DISTRIBUTIONLOCATION:
      return {
        ...state,
        distributionLocationEdit: {}
      };
    case DELETE_DISTRIBUTIONLOCATION_SUCCEEDED:
      const distributionLocationListindex = state.distributionLocationList.findIndex(
        obj => obj.id === action.data.id
      );

      // make new object of updated object.
      const distributionLocationListrecentObj = { ...action.data };

      // make final new array of objects by combining updated object.
      const afterDeletedistributionLocationList = [
        ...state.distributionLocationList.slice(
          0,
          distributionLocationListindex
        ),
        distributionLocationListrecentObj
      ];
      return {
        ...state,
        isSubmitSuccess: true,
        distributionLocationList: afterDeletedistributionLocationList
      };
    default:
      return state;
  }
}
