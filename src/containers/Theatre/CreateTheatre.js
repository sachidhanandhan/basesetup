import React from "react";
import { Field, reduxForm } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";

import {
  renderTextField,
  renderSelectField,
  CheckBoxInput
} from "../../components/FormFields";

class MaterialUiForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      distributionLocationList: [],
      subDistributionLocationList: [],
      stationLocationList: [],
      is_air_conditioned: true
    };
  }
  static getDerivedStateFromProps(props, state) {
    let distributionList = [];
    let subDistributionList = [];
    let stationList = [];

    // props.distributionLocationList.map(list => {
    //   distributionList.push({ label: list.name, value: list.id });
    // });
    // props.subDistributionLocationList.map(list => {
    //   subDistributionList.push({ label: list.name, value: list.id });
    // });
    // props.stationLocationList.map(list => {
    //   stationList.push({ label: list.name, value: list.id });
    // });
    // return {
    //   distributionLocationList: distributionList,
    //   subDistributionLocationList: subDistributionList,
    //   stationLocationList: stationList
    // };
  }

  handleChange = () => {
    const { is_air_conditioned } = this.state;
    this.setState({ is_air_conditioned: !is_air_conditioned });
  };

  render() {
    const {
      handleSubmit,
      pristine,
      reset,
      submitting,
      onSubmit,
      invalid,
      openForm,
      closeForm,
      classes,
      distributionLocationList,
      subDistributionLocationList,
      stationLocationList
    } = this.props;

    const { is_air_conditioned } = this.state;
    console.log("distributionLocationList=============");
    console.log(distributionLocationList);

    return (
      <FloatingPanel openForm={openForm} closeForm={closeForm}>
        <form className={classes.theatreForm} onSubmit={handleSubmit(onSubmit)}>
          <div className={classes.fields}>
            <Field
              name="name"
              component={renderTextField}
              label="Theatre Name"
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="mobile_number"
              component={renderTextField}
              label="Mobile Number"
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="phone_number"
              component={renderTextField}
              label="Phone Number"
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="manager_name"
              component={renderTextField}
              label="Manager Name"
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="station_name"
              component={renderTextField}
              label="Station Name"
            />
          </div>
          <div className={classes.fields}>
            <Field name="lat" component={renderTextField} label="Latitude" />
          </div>
          <div className={classes.fields}>
            <Field name="long" component={renderTextField} label="Longitude" />
          </div>
          <div className={classes.fields}>
            <Field
              name="distributionLocation"
              component={renderSelectField}
              options={distributionLocationList}
              label="Select Distribution"
              getOptionLabel={option => `${option.name}`}
              getOptionValue={option => `${option.id}`}
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="subdistributionLocation"
              component={renderSelectField}
              options={subDistributionLocationList}
              label="Select SubDistribution"
              getOptionLabel={option => `${option.name}`}
              getOptionValue={option => `${option.id}`}
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="station"
              component={renderSelectField}
              options={stationLocationList}
              label="Select Station"
              getOptionLabel={option => `${option.name}`}
              getOptionValue={option => `${option.id}`}
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="door_number"
              component={renderTextField}
              label="Door NO"
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="street_name"
              component={renderTextField}
              label="Street Details"
            />
          </div>
          <div className={classes.fields}>
            <Field name="area" component={renderTextField} label="Area" />
          </div>
          <div className={classes.fields}>
            <Field name="city" component={renderTextField} label="City" />
          </div>
          <div className={classes.fields}>
            <Field
              name="pincode"
              type="number"
              component={renderTextField}
              label="Pincode"
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="type"
              type="number"
              component={renderTextField}
              label="Type"
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="is_air_conditioned"
              component={CheckBoxInput}
              label="Is this AC Theatre?"
            />
          </div>
          <div>
            <Button
              className={classes.submitButton}
              type="submit"
              variant="contained"
              disabled={invalid}
            >
              Submit
            </Button>
            <Button
              className={classes.cleatButton}
              variant="contained"
              disabled={pristine || submitting}
              onClick={reset}
            >
              Clear
            </Button>
          </div>
        </form>
      </FloatingPanel>
    );
  }
}

let TheatreForm = reduxForm({
  form: "CreateTheatre", // a unique identifier for this form
  validate,
  enableReinitialize: true
})(MaterialUiForm);

TheatreForm = connect(state => ({
  initialValues: state.theatreCRUD.theatreEdit
}))(TheatreForm);
export default compose(withStyles(formStyle))(TheatreForm);
