const styles = theme => ({
  theatreContainor: {
    margin: "100px",
    padding: "40px"
  },
  eachRow: {
    margin: "10px",
    padding: "40px"
  },
  theatreTable: {
    borderRadius: "15px"
  },
  create: {
    display: "block"
  },
  createTheatre: {
    marginRight: "150px",
    float: "right"
  }
});

export default styles;
