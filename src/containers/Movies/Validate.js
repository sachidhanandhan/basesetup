const validate = values => {
  const errors = { company: {}, user: {} };

  if (values.company && !values.company.companyName) {
    errors.company.companyName = "Required";
  }
  if (values.company && !values.company.address) {
    errors.company.address = "Required";
  }
  if (values.company && !values.company.email) {
    errors.company.email = "Required";
  } else if (values.company) {
    if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.company.email)
    ) {
      errors.company.email = "Invalid email address";
    }
  }

  return errors;
};

export default validate;
