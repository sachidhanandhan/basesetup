const styles = theme => ({
  scheduleContainor: {
    margin: "100px",
    padding: "40px"
  },
  eachRow: {
    margin: "10px",
    padding: "40px"
  },
  scheduleTable: {
    borderRadius: "15px"
  },
  create: {
    display: "block"
  },
  createSchedule: {
    marginRight: "150px",
    float: "right"
  }
});

export default styles;
