import React from "react";
import { Field, reduxForm } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";
import {
  renderTextField,
  renderSelectField
} from "../../components/FormFields";

const MaterialUiForm = props => {
  const {
    handleSubmit,
    subDistributionLocationList,
    pristine,
    reset,
    submitting,
    onSubmit,
    invalid,
    openForm,
    closeForm,
    classes
  } = props;

  let subDistributionLocations = [];
  console.log("subDistributionLocation List", subDistributionLocationList);
  subDistributionLocationList.map(value => {
    subDistributionLocations.push({
      label: value.name,
      value: value.id
    });
  });

  return (
    <FloatingPanel openForm={openForm} closeForm={closeForm}>
      <form
        className={classes.stationLocationForm}
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className={classes.fields}>
          <Field
            name="name"
            component={renderTextField}
            label="StationLocation Name"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="subdistributionLocation"
            component={renderSelectField}
            options={subDistributionLocationList}
            getOptionLabel={option => `${option.name}`}
            getOptionValue={option => `${option.id}`}
            label="Subdistribution Location"
          />
        </div>

        <div>
          <Button
            className={classes.submitButton}
            type="submit"
            variant="contained"
            disabled={invalid || pristine || submitting}
          >
            Submit
          </Button>
          <Button
            className={classes.cleatButton}
            variant="contained"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear
          </Button>
        </div>
      </form>
    </FloatingPanel>
  );
};

let StationLocationForm = reduxForm({
  form: "CreateStationLocation", // a unique identifier for this form
  validate,
  enableReinitialize: true
})(MaterialUiForm);

StationLocationForm = connect(state => ({
  initialValues: state.stationLocationCRUD.stationLocationEdit
}))(StationLocationForm);

export default compose(withStyles(formStyle))(StationLocationForm);
