import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
// import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ReactTable from "react-table";
import "react-table/react-table.css";

import CreateDistributionLocation from "./SubdistributionLocation";
import styles from "./styles";

import { action } from "../../utils/util";
import { ADD_NEW_DISTRIBUTIONLOCATION } from "../../actions/actionConstants";

class DistributionLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      initialValue: {}
    };
  }
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };
  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    this.setState({ openForm: true });
  };
  submitAction = value => {
    console.log("value===================");
    console.log(value);

    const updatedValue = {
      distribution_location_id: value.distribution.value,
      name: value.name
    };
    console.log("SubdistributionLocation updated value =>", updatedValue);

    // const { createDistributionLocation } = this.props;
    // createDistributionLocation(value);
    // this.setState({ openForm: false });
  };
  render() {
    const { distributionLocationList, classes } = this.props;
    const { openForm } = this.state;

    return (
      <>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createDistributionLocation}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.distributionLocationContainor}>
          <ReactTable
            data={distributionLocationList}
            columns={[
              {
                Header: "Name",
                className: classes.eachRow,
                accessor: "name"
              }
            ]}
            defaultSorted={[
              {
                id: "name",
                desc: true
              }
            ]}
            defaultPageSize={distributionLocationList.length || 3}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${
              classes.distributionLocationTable
            }`}
          />
        </div>

        <CreateDistributionLocation
          distributionLocationList={distributionLocationList}
          openForm={openForm}
          closeForm={this.closeForm}
          onSubmit={this.submitAction}
        />
      </>
    );
  }
}

DistributionLocation.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  distributionLocationList: PropTypes.array.isRequired
};
const mapDispatchToProps = dispatch => ({
  createDistributionLocation: value => {
    dispatch(action(ADD_NEW_DISTRIBUTIONLOCATION, value));
  }
});
const DistributionLocationCmpt = connect(
  null,
  mapDispatchToProps
)(DistributionLocation);

export default compose(withStyles(styles))(DistributionLocationCmpt);
