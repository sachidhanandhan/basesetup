const styles = theme => ({
  distributionLocationContainor: {
    margin: "100px",
    padding: "40px"
  },
  eachRow: {
    margin: "10px",
    padding: "40px"
  },
  distributionLocationTable: {
    borderRadius: "15px"
  },
  create: {
    display: "block"
  },
  createDistributionLocation: {
    marginRight: "150px",
    float: "right"
  }
});

export default styles;
