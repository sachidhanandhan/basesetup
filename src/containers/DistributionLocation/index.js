import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
// import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ReactTable from "react-table";
import "react-table/react-table.css";

import CreateDistributionLocation from "./DistributionLocationCreate";
import styles from "./styles";

import { action } from "../../utils/util";
import {
  ADD_NEW_DISTRIBUTIONLOCATION,
  UPDATE_DISTRIBUTIONLOCATION,
  DELETE_DISTRIBUTIONLOCATION,
  FETCH_DISTRIBUTIONLOCATION
} from "../../actions/actionConstants";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  editDistributionLocation,
  clearDistributionLocation
} from "../../actions/admin";

class DistributionLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openForm: false,
      initialValue: {}
    };
  }
  componentDidMount() {
    const { fetchDistributionLocation } = this.props;
    fetchDistributionLocation();
  }

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };
  closeForm = () => {
    this.setState({ openForm: false });
  };
  openForm = () => {
    this.setState({ openForm: true });
  };
  submitAction = value => {
    const {
      createDistributionLocation,
      updateDistributionLocation
    } = this.props;
    console.log(
      "DistributionLocaion container => before value.id submitAction",
      value
    );
    if (value.id) {
      console.log(
        "DistributionLocaion container => updateDistributionLocation()",
        value
      );
      updateDistributionLocation(value);
    } else {
      createDistributionLocation(value);
    }

    this.setState({ openForm: false });
  };
  handleEdit = rowData => {
    console.log(
      "Distribution Location Container => index => handleEdit()",
      rowData.original
    );
    const { editDistributionLocationDetails } = this.props;
    editDistributionLocationDetails(rowData.original);
    this.setState({ initialValue: rowData.original, openForm: true });
  };
  handleDelete = rowData => {
    console.log(
      "Distribution Location Container => index => handleDete()",
      rowData
    );
    const { deleteDistributionLocationDetails } = this.props;
    deleteDistributionLocationDetails(rowData.original);
  };
  render() {
    const { distributionLocationList, classes } = this.props;
    const { openForm, initialValue } = this.state;
    const tableLength = distributionLocationList.length
      ? distributionLocationList.length
      : 3;

    return (
      <>
        <div className={classes.create}>
          <div>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createDistributionLocation}
              onClick={this.openForm}
            >
              Add
            </Button>
          </div>
        </div>
        <div className={classes.distributionLocationContainor}>
          <ReactTable
            data={distributionLocationList}
            columns={[
              {
                Header: "",
                className: classes.eachRow,
                Cell: rowData => (
                  <div>
                    <EditIcon onClick={() => this.handleEdit(rowData)} />
                    <DeleteIcon onClick={() => this.handleDelete(rowData)} />
                  </div>
                ),
                minWidth: 20
              },
              {
                Header: "Name",
                className: classes.eachRow,
                accessor: "name"
              }
            ]}
            defaultSorted={[
              {
                id: "name",
                desc: true
              }
            ]}
            pageSize={tableLength}
            showPagination={false}
            resizable={false}
            style={{ maxHeight: 400 }}
            className={`-striped -highlight ${
              classes.distributionLocationTable
            }`}
          />
        </div>

        <CreateDistributionLocation
          openForm={openForm}
          closeForm={this.closeForm}
          initialValue={initialValue}
          onSubmit={this.submitAction}
        />
      </>
    );
  }
}

DistributionLocation.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
  distributionLocationList: PropTypes.array.isRequired
};
const mapStateToProps = state => ({
  distributionLocationList:
    state.distributionLocationCRUD.distributionLocationList
});
const mapDispatchToProps = dispatch => ({
  fetchDistributionLocation: () => {
    dispatch(action(FETCH_DISTRIBUTIONLOCATION));
  },
  createDistributionLocation: value => {
    dispatch(action(ADD_NEW_DISTRIBUTIONLOCATION, value));
  },
  updateDistributionLocation: value => {
    dispatch(action(UPDATE_DISTRIBUTIONLOCATION, value));
  },
  editDistributionLocationDetails: value => {
    dispatch(editDistributionLocation(value));
  },
  clearDistributionLocation: () => {
    dispatch(clearDistributionLocation());
  },
  deleteDistributionLocationDetails: value => {
    dispatch(action(DELETE_DISTRIBUTIONLOCATION, value));
  }
});
const DistributionLocationCmpt = connect(
  mapStateToProps,
  mapDispatchToProps
)(DistributionLocation);

export default compose(withStyles(styles))(DistributionLocationCmpt);
