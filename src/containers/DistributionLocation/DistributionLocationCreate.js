import React from "react";
import { Field, reduxForm } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";

import { renderTextField } from "../../components/FormFields";

const MaterialUiForm = props => {
  const {
    handleSubmit,
    pristine,
    reset,
    submitting,
    onSubmit,
    invalid,
    openForm,
    closeForm,
    classes
  } = props;

  return (
    <FloatingPanel openForm={openForm} closeForm={closeForm}>
      <form
        className={classes.distributionLocationForm}
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className={classes.fields}>
          <Field
            name="name"
            component={renderTextField}
            label="DistributionLocation Name"
          />
        </div>

        <div>
          <Button
            className={classes.submitButton}
            type="submit"
            variant="contained"
            disabled={invalid || pristine || submitting}
          >
            Submit
          </Button>
          <Button
            className={classes.cleatButton}
            variant="contained"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear
          </Button>
        </div>
      </form>
    </FloatingPanel>
  );
};

let DistributionLocationForm = reduxForm({
  form: "CreateDistributionLocation", // a unique identifier for this form
  validate,
  enableReinitialize: true
})(MaterialUiForm);

DistributionLocationForm = connect(state => ({
  initialValues: state.distributionLocationCRUD.distributionLocationEdit
}))(DistributionLocationForm);

export default compose(withStyles(formStyle))(DistributionLocationForm);
