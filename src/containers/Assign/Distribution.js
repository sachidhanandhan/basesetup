import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Field } from "redux-form";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { renderSelectField } from "../../components/FormFields";
import styles from "./styles/formStyle";

const renderSlots = ({
  fields,
  classes,
  companyList,
  distributonAreaList,
  meta: { error, submitFailed }
}) => (
  <ul className={classes.multifields}>
    <li>
      <Button
        size="small"
        variant="outlined"
        color="primary"
        onClick={() => fields.push({})}
      >
        Add Distribution
      </Button>
      {submitFailed && error && <span>{error}</span>}
    </li>
    {fields.map((distribution, index) => (
      <li key={index}>
        <Grid container md={12} xs={12}>
          <Grid item md={6} xs={6} className={classes.multifieldsGrid}>
            <h4>Distribution #{index + 1}</h4>
          </Grid>
          <Grid item md={1} xs={6} className={classes.multifieldsGrid}>
            <IconButton
              aria-label="Delete"
              className={classes.margin}
              onClick={() => fields.remove(index)}
            >
              <DeleteIcon fontSize="small" />
            </IconButton>
          </Grid>

          <Grid item md={6} xs={6} className={classes.multifieldsGrid}>
            <Field
              className={classes.movieSelect}
              name={`${distribution}.areaId`}
              component={renderSelectField}
              options={distributonAreaList}
              label="Distributer Area"
              getOptionLabel={option => `${option.name}`}
              getOptionValue={option => `${option.id}`}
            />
          </Grid>
          <Grid item md={6} xs={6} className={classes.multifieldsGrid}>
            <Field
              className={classes.movieSelect}
              name={`${distribution}.companyId`}
              component={renderSelectField}
              options={companyList}
              label="Distributer"
              getOptionLabel={option => `${option.name}`}
              getOptionValue={option => `${option.id}`}
            />
          </Grid>
        </Grid>
      </li>
    ))}
  </ul>
);

export default withStyles(styles)(renderSlots);
