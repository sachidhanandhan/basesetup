import React from "react";
import { Field, FieldArray, reduxForm, FormSection } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";
import renderDistribution from "./Distribution";
import renderSubdistribution from "./Subdistribution";

import { renderSelectField } from "../../components/FormFields";

const AssignMovieForm = props => {
  const {
    companyList,
    distributonAreaList,
    subdistributonAreaList,
    movieList,
    handleSubmit,
    pristine,
    reset,
    submitting,
    onSubmit,
    invalid,
    openForm,
    closeForm,
    classes
  } = props;
  let isEdit = false;
  console.log("movie list is", movieList);
  console.log("company list is", companyList);

  return (
    <FloatingPanel
      openForm={openForm}
      closeForm={closeForm}
      style={{ width: "600px" }}
    >
      <form className={classes.assignForm} onSubmit={handleSubmit(onSubmit)}>
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="movie"
            component={renderSelectField}
            options={movieList}
            label="Movie"
            getOptionLabel={option => `${option.name}`}
            getOptionValue={option => `${option.id}`}
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="producer"
            component={renderSelectField}
            options={companyList}
            label="Producer"
            getOptionLabel={option => `${option.name}`}
            getOptionValue={option => `${option.id}`}
          />
        </div>
        <FieldArray
          name="distribution"
          companyList={companyList}
          distributonAreaList={distributonAreaList}
          component={renderDistribution}
        />
        <FieldArray
          name="subDistribution"
          companyList={companyList}
          subdistributonAreaList={subdistributonAreaList}
          component={renderSubdistribution}
        />

        <div>
          <Button
            className={classes.submitButton}
            type="submit"
            variant="contained"
            disabled={invalid}
          >
            Submit
          </Button>
          <Button
            className={classes.cleatButton}
            variant="contained"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear
          </Button>
        </div>
      </form>
    </FloatingPanel>
  );
};

let AssignForm = reduxForm({
  form: "AssignMovie", // a unique identifier for this form
  validate,
  enableReinitialize: true
})(AssignMovieForm);

AssignForm = connect(state => ({
  initialValues: state.assignCRUD.assignEdit || {}
}))(AssignForm);
export default compose(withStyles(formStyle))(AssignForm);
