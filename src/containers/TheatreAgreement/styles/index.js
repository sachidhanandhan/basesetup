const styles = theme => ({
  theatreAgreementContainor: {
    margin: "100px",
    padding: "40px"
  },
  eachRow: {
    margin: "10px",
    padding: "40px"
  },
  theatreAgreementTable: {
    borderRadius: "15px"
  },
  create: {
    display: "block"
  },
  createTheatreAgreement: {
    marginRight: "150px",
    float: "right"
  }
});

export default styles;
