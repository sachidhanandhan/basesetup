import React from "react";
import { Field, reduxForm } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";
import {
  renderTextField,
  renderSelectField,
  DateTimePickerRow,
  renderCreatableSelect,
  CheckBoxInput
} from "../../components/FormFields";

class MaterialUiForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_air_conditioned: true
    };
  }
  handleChange = () => {
    const { is_air_conditioned } = this.state;
    this.setState({ is_air_conditioned: !is_air_conditioned });
  };
  render() {
    const {
      handleSubmit,
      movieList,
      theatreList,
      companyList,
      pristine,
      reset,
      submitting,
      onSubmit,
      invalid,
      openForm,
      closeForm,
      classes
    } = this.props;

    let movies = [];
    let companies = [];
    let theatres = [];
    const theatreType = [
      { label: "Type1", value: 1 },
      { label: "Type2", value: 2 }
    ];

    movieList.map(value => {
      movies.push({
        label: value.name,
        value: value.id
      });
    });
    companyList.map(value => {
      companies.push({
        label: value.name,
        value: value.id
      });
    });
    theatreList.map(value => {
      theatres.push({
        label: value.name,
        value: value.id
      });
    });
    return (
      <FloatingPanel openForm={openForm} closeForm={closeForm}>
        <form className={classes.theatreForm} onSubmit={handleSubmit(onSubmit)}>
          <div className={classes.fields}>
            <Field name="tax" component={renderTextField} label="Tax" />
          </div>
          <div className={classes.fields}>
            <Field
              name="created_date"
              component={DateTimePickerRow}
              label="Created Date"
              dateFormat="dd/MM/yyyy"
            />
          </div>
          <div className={classes.fields}>
            <Field
              className={classes.movieSelect}
              name="type"
              component={renderSelectField}
              options={theatreType}
              label="Theatre Type"
            />
          </div>
          <div className={classes.fields}>
            <Field
              className={classes.movieSelect}
              name="movie"
              component={renderSelectField}
              options={movieList}
              getOptionLabel={option => `${option.name}`}
              getOptionValue={option => `${option.id}`}
              label="Select Movie"
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="share"
              component={renderCreatableSelect}
              label="Share"
            />
          </div>
          <div className={classes.fields}>
            <Field
              name="ac"
              component={CheckBoxInput}
              label="Is this AC Theatre?"
            />
          </div>
          <div className={classes.fields}>
            <Field
              className={classes.movieSelect}
              name="theater"
              component={renderSelectField}
              options={theatreList}
              getOptionLabel={option => `${option.name}`}
              getOptionValue={option => `${option.id}`}
              label="Select Theatre"
            />
          </div>
          <div className={classes.fields}>
            <Field
              className={classes.movieSelect}
              name="company"
              component={renderSelectField}
              options={companyList}
              getOptionLabel={option => `${option.name}`}
              getOptionValue={option => `${option.id}`}
              label="Select Company"
            />
          </div>

          <div>
            <Button
              className={classes.submitButton}
              type="submit"
              variant="contained"
              disabled={invalid || pristine || submitting}
            >
              Submit
            </Button>
            <Button
              className={classes.cleatButton}
              variant="contained"
              disabled={pristine || submitting}
              onClick={reset}
            >
              Clear
            </Button>
          </div>
        </form>
      </FloatingPanel>
    );
  }
}

let TheatreAgreementCRUDForm = reduxForm({
  form: "CreateTheatre", // a unique identifier for this form
  validate,
  enableReinitialize: true
})(MaterialUiForm);

TheatreAgreementCRUDForm = connect(state => ({
  initialValues: state.theatreAgreementCRUD.agreementEdit
}))(TheatreAgreementCRUDForm);

export default compose(withStyles(formStyle))(TheatreAgreementCRUDForm);
