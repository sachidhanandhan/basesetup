import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { Field, FieldArray, reduxForm } from "redux-form";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import ReactTable from "react-table";
import "react-table/react-table.css";

import validate from "./validate";
import styles from "./styles/styles";
import {
  renderSelectField,
  DateTimePickerRow
} from "../../components/FormFields";
import renderSlots from "./Multifields";
// import { emptyFunction, action } from "../../utils/util";
import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { action } from "../../utils/util";
import {
  FETCH_MOVIE,
  FETCH_THEATRE_BY_MOVIE,
  ADD_NEW_COLLECTION,
  FETCH_COLLECTION
} from "../../actions/actionConstants";
import { editCollection } from "../../actions/admin";

class CollectionArraysForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date()
    };
  }
  componentDidMount() {
    const { fetchMovie } = this.props;
    fetchMovie();
  }
  onSubmit = value => {
    const { createCollection } = this.props;
    const { startDate, movie } = this.state;

    const currentDate = new Date(startDate);
    const date = currentDate.getDate();
    const month = currentDate.getMonth(); //Be careful! January is 0 not 1
    const year = currentDate.getFullYear();
    const time = value.slotTime.toLocaleTimeString("it-IT");
    const dateString = year + "-" + (month + 1) + "-" + date;
    let submitData;
    if (value.movie) {
      submitData = {
        movie_id: value.movie.id,
        company_id: 1,
        theater_id: value.theatre.id,
        date: value.date,
        slotTime: time,
        slot: value.slot
      };
    } else {
      submitData = {
        movie_id: movie.id,
        company_id: 1,
        theater_id: value.theatre.id,
        date: dateString,
        slotTime: time,
        slot: value.slot
      };
    }

    createCollection(submitData);
  };
  handleChange = val => {
    this.setState({ startDate: val });
  };
  handleSelectChange = val => {
    this.setState({ movie: val });
  };
  handleEdit = rowData => {
    const { editCollectionDetails } = this.props;
    editCollectionDetails(rowData.original);
    this.setState({ initialValue: rowData.original, openForm: true });
  };
  searchTheatre = () => {
    const { fetchTheatreByMovie, fetchCollectionByMovie } = this.props;
    const param = {
      movieId: this.state.movie.id,
      companyId: 14,
      date: this.state.startDate
    };
    console.log("param");
    console.log(this.state.movie);

    fetchTheatreByMovie(param);
    fetchCollectionByMovie(param);
  };
  render() {
    const {
      handleSubmit,
      pristine,
      reset,
      submitting,
      movieList,
      theatreList,
      collectionList,
      classes
    } = this.props;

    const { startDate, movie } = this.state;
    // const theatreList = []
    // const movieList = [
    //   { label: "movie 1", value: "1" },
    //   { label: "movie 2", value: "2" }
    // ];
    console.log("collectionList------------");
    console.log(collectionList);

    const tableLength = collectionList.length;
    return (
      <Grid container className={classes.rootContainer} spacing={2}>
        <Grid container md={12} xs={12} className={classes.topContainer}>
          <Grid item md={4} xs={4}>
            <label className={classes.label}>Date:</label>
            <DatePicker
              selected={startDate}
              onChange={this.handleChange}
              dateFormat="dd/MM/yyyy"
            />
          </Grid>
          <Grid item md={6} xs={4}>
            <label className={classes.label}>Movie:</label>
            <Select
              className={classes.movieSelect}
              value={movie}
              isClearable={true}
              isSearchable={true}
              name="movie"
              onChange={this.handleSelectChange}
              isMulti={false}
              options={movieList}
              getOptionLabel={option => `${option.name}`}
              getOptionValue={option => `${option.id}`}
            />
          </Grid>
          <Grid item md={2} xs={4}>
            <Button
              variant="outlined"
              color="primary"
              className={classes.createCompany}
              onClick={this.searchTheatre}
            >
              Search
            </Button>
          </Grid>
        </Grid>
        <Grid item md={6} xs={12}>
          <div className={classes.collectContainor}>
            <form onSubmit={handleSubmit(this.onSubmit)}>
              <Grid container md={12} xs={12}>
                <Grid item md={5} xs={12}>
                  <label className={classes.label}>Time:</label>
                  <Field
                    name="slotTime"
                    component={DateTimePickerRow}
                    label=""
                    showTimeSelect
                    showTimeSelectOnly
                    timeIntervals={15}
                    dateFormat="h:mm aa"
                    timeCaption="Time"
                  />
                </Grid>
                <Grid item md={7} xs={12}>
                  <label className={classes.label}>Theatre:</label>
                  <Field
                    className={classes.movieSelect}
                    name="theatre"
                    component={renderSelectField}
                    options={theatreList}
                    label="Select"
                    getOptionLabel={option => `${option.name}`}
                    getOptionValue={option => `${option.id}`}
                  />
                </Grid>
              </Grid>

              <FieldArray name="slot" component={renderSlots} />
              <div>
                <Button
                  size="small"
                  variant="outlined"
                  color="primary"
                  type="submit"
                  disabled={submitting}
                >
                  Submit
                </Button>
                <Button
                  size="small"
                  variant="outlined"
                  color="default"
                  disabled={pristine || submitting}
                  onClick={reset}
                >
                  Clear Values
                </Button>
              </div>
            </form>
          </div>
        </Grid>
        <Grid item md={6} xs={12}>
          {tableLength && (
            <ReactTable
              data={collectionList}
              columns={[
                {
                  Header: "",
                  className: classes.eachRow,
                  Cell: rowData => (
                    <div>
                      <EditIcon onClick={() => this.handleEdit(rowData)} />
                      {/* <DeleteIcon onClick={() => this.handleDelete(rowData)} /> */}
                    </div>
                  ),
                  minWidth: 40
                },
                {
                  Header: "movie",
                  className: classes.eachRow,
                  accessor: "movie.name"
                },
                {
                  Header: "theatre",
                  className: classes.eachRow,
                  accessor: "theatre.name"
                },
                {
                  Header: "date",
                  className: classes.eachRow,
                  accessor: "date"
                },
                {
                  Header: "slotTime",
                  className: classes.eachRow,
                  accessor: "slotTime"
                }
              ]}
              pageSize={tableLength}
              showPagination={false}
              resizable={false}
              style={{ maxHeight: 400 }}
              className={`-striped -highlight ${classes.companyTable}`}
            />
          )}
        </Grid>
      </Grid>
    );
  }
}

let CollectionForm = reduxForm({
  form: "CollectionForm", // a unique identifier for this form
  validate,
  enableReinitialize: true
})(CollectionArraysForm);
CollectionForm = connect(state => ({
  initialValues: state.collectionCRUD.collectionEdit
}))(CollectionForm);
const mapStateToProps = state => ({
  movieList: state.movieCRUD.movieList,
  theatreList: state.collectionCRUD.theatreList,
  collectionList: state.collectionCRUD.collectionList
});
const mapDispatchToProps = dispatch => ({
  fetchMovie: () => {
    dispatch(action(FETCH_MOVIE));
  },
  fetchTheatreByMovie: value => {
    dispatch(action(FETCH_THEATRE_BY_MOVIE, value));
  },
  fetchCollectionByMovie: value => {
    dispatch(action(FETCH_COLLECTION, value));
  },
  createCollection: value => {
    dispatch(action(ADD_NEW_COLLECTION, value));
  },
  editCollectionDetails: value => {
    dispatch(editCollection(value));
  }
});
const CollectionComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(CollectionForm);
export default compose(withStyles(styles))(CollectionComponent);
