const styles = theme => ({
    collectContainor: {
        // margin: '40px 40px'

    },
    movieSelect: {
        width: '80%',
        display:'inline-block'
    },
    topContainer:{
      borderRadius:'10px',
      border:'1px solid #2196f3',
      padding:'10px 5px',
      marginBottom:'20px',
    },
    label:{
      paddingRight: '10px',
      'line-height': 1.75,
      'font-weight': 500,
      'font-family': 'Roboto Helvetica Arial sans-serif'
    },
    multifields:{
      padding:0,
      '& li': {
        // color:'red',
        display:'block',

        '& button': {
          // color:'red'
        }
      }
    },
    multifieldsGrid: {
      paddingRight:'2px',
      '& button': {
        marginTop:'15px'
      }
    }
  });
  
  export default styles;
  