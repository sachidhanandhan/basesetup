import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles/style";
import Company from "../Company";
import Theatre from "../Theatre";
// import Movie from "../Movie";
import DistributionLocation from "../DistributionLocation";
import StationLocation from "../StationLocation";
import SubDistributionLocation from "../SubDistributionLocation";
import TheatreAgreement from "../TheatreAgreement";
import Schedule from "../Schedule";
import Movie from "../Movies";

import { action } from "../../utils/util";
import {
  FETCH_MOVIE,
  FETCH_THEATRE,
  FETCH_DISTRIBUTIONLOCATION,
  FETCH_SUBDISTRIBUTIONLOCATION,
  FETCH_STATIONLOCATION,
  FETCH_SCHEDULE,
  FETCH_AGREEMENT
} from "../../actions/actionConstants";
class Home extends Component {
  componentDidMount() {
    const {
      fetchMovie,
      fetchStationLocation,
      fetchDistributionLocation,
      fetchSubDistributionLocation,
      fetchTheatre,
      fetchSchedule,
      fetchTheatreAgreement
    } = this.props;
    fetchMovie();
    fetchStationLocation();
    fetchDistributionLocation();
    fetchSubDistributionLocation();
    fetchTheatre();
    fetchSchedule();
    fetchTheatreAgreement();
  }

  render() {
    const { classes, theatreList } = this.props;

    console.log("Home Component =>", theatreList);

    return (
      <div>
        <h2 className={classes.TableTile}>Company</h2>
        <Company />
        <h2 className={classes.TableTile}> Movie</h2>
        <Movie />

        <h2 className={classes.TableTile}> Theatre</h2>
        <Theatre />
        <h2 className={classes.TableTile}>Distribution Location</h2>
        <DistributionLocation />
        <h2 className={classes.TableTile}>Sub Distribution Location</h2>
        <SubDistributionLocation />
        <h2 className={classes.TableTile}>Station Location</h2>
        <StationLocation />
        <h2 className={classes.TableTile}>Theatre Agreement</h2>
        <TheatreAgreement />
        <h2 className={classes.TableTile}>Schedule</h2>
        <Schedule />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  companyList: state.companyCRUD.companyList,
  movieList: state.movieCRUD.movieList,
  stationLocationList: state.stationLocationCRUD.stationLocationList,
  scheduleList: state.scheduleCRUD.scheduleList,
  theatreAgreementList: state.theatreAgreementCRUD.theatreList,
  distributionLocationList:
    state.distributionLocationCRUD.distributionLocationList,
  subDistributionLocationList:
    state.subDistributionLocationCRUD.subDistributionLocationList,
  theatreList: state.theatreCRUD.theatreList
});
const mapDispatchToProps = dispatch => ({
  fetchMovie: () => {
    dispatch(action(FETCH_MOVIE));
  },
  fetchTheatre: () => {
    dispatch(action(FETCH_THEATRE));
  },
  fetchDistributionLocation: () => {
    dispatch(action(FETCH_DISTRIBUTIONLOCATION));
  },
  fetchSubDistributionLocation: () => {
    dispatch(action(FETCH_SUBDISTRIBUTIONLOCATION));
  },
  fetchStationLocation: () => {
    dispatch(action(FETCH_STATIONLOCATION));
  },
  fetchSchedule: () => {
    dispatch(action(FETCH_SCHEDULE));
  },
  fetchTheatreAgreement: () => {
    dispatch(action(FETCH_AGREEMENT));
  }
});
const HomeComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
export default compose(withStyles(styles))(HomeComponent);
