import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import 'date-fns';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { 
    Button,
    Popover,
    Typography,
    FormControl,
    Input,
    Select,
    MenuItem,
    InputLabel,
    FormHelperText
 } from '@material-ui/core';
// import DateFnsUtils from '@date-io/date-fns';
// import { addDays } from 'date-fns'
import { MuiPickersUtilsProvider, TimePicker, InlineDatePicker } from 'material-ui-pickers';
import styles from "./styles/search";
import { renderTextField } from "../../components/TextField";
class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: null,
            endDate:null,
            anchorEl: null,
            guests:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
            rooms:[1,2,3,4,5,6,7,8,9,10],
            guest:2,
            room:1
          };
        
          
    }
    handleendDateChange = date => {
        this.setState({ endDate: date });
      };
      handlestartDateChange = date => {
        
        this.setState({ startDate: date });
      };

      handleClick = event => {
        this.setState({
          anchorEl: event.currentTarget,
        });
      };
    
      handleClose = () => {
        this.setState({
          anchorEl: null,
        });
      };

    render() { 
        const { classes } = this.props;
        const { startDate,endDate,anchorEl,guests,guest,rooms,room } = this.state;
    const open = Boolean(anchorEl);
        return (
         <div className={classes.searchBar}>
            <h3>Book Domestic & International Hotels</h3>
              {/* <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <InlineDatePicker label="Check In:" className={classes.date}  minDate={ new Date()}  value={startDate} onChange={this.handlestartDateChange} />
                <InlineDatePicker label="Check Out:" className={classes.date}  minDate={startDate} value={endDate} onChange={this.handleendDateChange} />
            </MuiPickersUtilsProvider> */}

            <Button
                variant="outlined" 
                className={classes.bookingButton} 
                aria-owns={open ? 'simple-popper' : undefined}
                aria-haspopup="true"
                variant="contained"
                onClick={this.handleClick}>
                2 Guest / 1 room
            </Button>
  
        <Popover
          id="simple-popper"
         
          open={open}
          anchorEl={anchorEl}
          onClose={this.handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <div  className={classes.guestPopOver} >
          <FormControl className={classes.formControl}>
          <InputLabel htmlFor="guests">Guests</InputLabel>
          <Select
            value={this.state.guest}
            onChange={this.handlegGestChange}
            input={<Input name="guests" id="guests" />}
          >
            
            {
                guests.map((num)=>{
                    return(
                        <MenuItem value={num} className={classes.guestMenu}>
                        {num}
                        </MenuItem>
                    )
                })
            }
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="rooms">Rooms</InputLabel>
          <Select
            value={this.state.room}
            onChange={this.handleRoomChange}
            input={<Input name="rooms" id="rooms" />}
          >
            {
                rooms.map((num)=>{
                    return(
                        <MenuItem value={num} className={classes.roomMenu}>
                        {num}
                        </MenuItem>
                    )
                })
            }
          </Select>
        </FormControl>
          </div>
        </Popover>
         </div>
        );
    }
}

Search.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(Search);