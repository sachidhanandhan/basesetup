const styles = theme => ({
    searchBar:{
        padding:"10px",
        border:"1px solid",
        borderRadius:"10px"
    },
    guestPopOver: {
      padding:"20px",
    },
    formControl: {
        margin:"10px",
    },
    guestMenu: {
        padding:"5px"
    },
    roomMenu: {
        padding:"5px"
    },
    date:{
        marginRight:"10px"
    },
    bookingButton: {
        marginTop:"10px"
    },
  });
  
  export default styles;
  