import React from "react";
import { Field, reduxForm } from "redux-form";
import Button from "@material-ui/core/Button";
import { compose } from "redux";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import validate from "./Validate";
import FloatingPanel from "../../components/Panel/FloatingPanel";
import formStyle from "./styles/formStyle";

import {
  renderTextField,
  renderSelectField
} from "../../components/FormFields";

const CreateUserForm = props => {
  const {
    companyList,
    handleSubmit,
    pristine,
    reset,
    submitting,
    onSubmit,
    invalid,
    openForm,
    closeForm,
    classes
  } = props;
  const roles = [
    { label: "admin", id: 1, value: 1 },
    { label: "collection Agent", id: 2, value: 2 }
  ];
  // let company = [];
  console.log("companyList=======================");
  console.log(companyList);

  // companyList.map(value => {
  //   company.push({ label: value.name, value: value.id });
  // });
  return (
    <FloatingPanel openForm={openForm} closeForm={closeForm}>
      <form className={classes.userForm} onSubmit={handleSubmit(onSubmit)}>
        <div className={classes.fields}>
          <Field
            className={classes.field}
            name="name"
            component={renderTextField}
            label="Name"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.field}
            name="username"
            type="number"
            component={renderTextField}
            label="Mobile Number"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.field}
            name="email"
            type="email"
            component={renderTextField}
            label="Email"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.field}
            name="password"
            type="password"
            component={renderTextField}
            label="Password"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.field}
            name="confirmPassword"
            type="password"
            component={renderTextField}
            label="Confirm Password"
          />
        </div>
        <div className={classes.fields}>
          <Field
            className={classes.movieSelect}
            name="company"
            component={renderSelectField}
            options={companyList}
            label="Company"
            getOptionLabel={option => `${option.name}`}
            getOptionValue={option => `${option.id}`}
          />
        </div>
        <div className={classes.fields}>
          <Field
            name="role"
            component={renderSelectField}
            options={roles}
            className={classes.selectClass}
            label="Select"
          />
        </div>

        <div>
          <Button
            className={classes.submitButton}
            type="submit"
            variant="contained"
            disabled={invalid}
          >
            Submit
          </Button>
          <Button
            className={classes.cleatButton}
            variant="contained"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Clear
          </Button>
        </div>
      </form>
    </FloatingPanel>
  );
};

let UserForm = reduxForm({
  form: "CreateUser", // a unique identifier for this form
  validate,
  enableReinitialize: true
})(CreateUserForm);

UserForm = connect(state => ({
  initialValues: state.userCRUD.userEdit || {}
}))(UserForm);
export default compose(withStyles(formStyle))(UserForm);
