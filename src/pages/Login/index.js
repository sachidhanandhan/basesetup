import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from "./styles/styles";
import { Redirect } from "react-router-dom";
import { USER_LOGIN } from "../../actions/actionConstants.js";
import { action } from "../../utils/util";

function SignIn(props) {
  const { classes, isLogIn } = props;
  const login = event => {
    event.preventDefault();
    const value = {
      username: event.target.username.value,
      password: event.target.password.value
    };
    console.log("Pages -> Login/index : value from input is ", value);

    props.login(value);
  };
  return isLogIn ? (
    <Redirect to="/" />
  ) : (
    <main className={classes.main}>
      <CssBaseline />
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} onSubmit={login}>
          <FormControl margin="normal" required fullWidth>
            <InputLabel>Username</InputLabel>
            <Input
              id="username"
              name="username"
              autoComplete="email"
              autoFocus
            />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              name="password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
          </FormControl>
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign in
          </Button>
        </form>
      </Paper>
    </main>
  );
}

SignIn.propTypes = {
  classes: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  isLogIn: state.user.isLogIn
});
const mapDispatchToProps = dispatch => ({
  login: value => {
    console.log("Pages -> Login/index : value from dispatchToProps is ", value);
    dispatch(action(USER_LOGIN, value));
  }
});
const LogIn = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);

export default compose(withStyles(styles))(LogIn);
