import { put, call, takeLatest } from "redux-saga/effects";
import {
  FETCH_USER,
  ADD_NEW_USER,
  UPDATE_USER,
  DELETE_USER
} from "../actions/actionConstants";
import Api from "../api/userCRUD";
import {
  fetchUserSuccess,
  createUserSuccess,
  updateUserSuccess,
  deleteUserSuccess
} from "../actions/admin";

// User
export function* fetchUser() {
  try {
    console.log("fetch userrrrrrrrrrrrrrrrrr saga called");
    const response = yield call(Api.fetchUser);
    yield put(fetchUserSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* createUser(value) {
  try {
    yield call(Api.createUser, value);
    yield put(createUserSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* updateUser(value) {
  try {
    yield call(Api.updateUser, value);
    yield put(updateUserSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteUser(value) {
  try {
    yield call(Api.deleteUser, value);
    yield put(deleteUserSuccess(value));
  } catch (error) {
    console.log(error);
  }
}

export const userCRUD = [
  takeLatest(FETCH_USER, fetchUser),
  takeLatest(ADD_NEW_USER, createUser),
  takeLatest(UPDATE_USER, updateUser),
  takeLatest(DELETE_USER, deleteUser)
];
