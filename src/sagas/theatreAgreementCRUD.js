import { put, call, takeLatest } from "redux-saga/effects";
import Api from "../api/theatreAgreementCRUD";
import {
  fetchAgreementSuccess,
  deleteAgreementSuccess,
  updateAgreementSuccess,
  createAgreementSuccess
} from "../actions/admin";
import {
  ADD_NEW_AGREEMENT,
  FETCH_AGREEMENT,
  UPDATE_AGREEMENT,
  DELETE_AGREEMENT
} from "../actions/actionConstants";

// Agreement
export function* createAgreement(value) {
  try {
    console.log("Create Agreement Saga Called");
    const response = yield call(Api.createagreement, value);
    yield put(createAgreementSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateAgreement(value) {
  try {
    console.log("update Agreement Saga Called");
    const response = yield call(Api.updateagreement, value);
    yield put(updateAgreementSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteAgreement(value) {
  try {
    console.log("delete Agreement Saga Called");
    yield call(Api.deleteagreement, value);
    yield put(deleteAgreementSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchAgreement() {
  try {
    console.log("fetch Agreement saga called");
    const response = yield call(Api.fetchagreement);
    yield put(fetchAgreementSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const theatreAgreementCRUD = [
  takeLatest(ADD_NEW_AGREEMENT, createAgreement),
  takeLatest(FETCH_AGREEMENT, fetchAgreement),
  takeLatest(UPDATE_AGREEMENT, updateAgreement),
  takeLatest(DELETE_AGREEMENT, deleteAgreement)
];
