import { put, call, takeLatest } from "redux-saga/effects";
import Api from "../api/theatreCRUD";
import {
  createTheatreSuccess,
  updateTheatreSuccess,
  deleteTheatreSuccess,
  fetchTheatreSuccess
} from "../actions/admin";
import {
  FETCH_THEATRE,
  ADD_NEW_THEATRE,
  UPDATE_THEATRE,
  DELETE_THEATRE
} from "../actions/actionConstants";

// Theatre
export function* createTheatre(value) {
  try {
    console.log("Create Theatre Saga Called");
    const response = yield call(Api.createtheatre, value);
    yield put(createTheatreSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateTheatre(value) {
  try {
    console.log("update Theatre Saga Called");
    const response = yield call(Api.updatetheatre, value);
    yield put(updateTheatreSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteTheatre(value) {
  try {
    console.log("delete Theatre Saga Called");
    yield call(Api.deletetheatre, value);
    yield put(deleteTheatreSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchTheatre() {
  try {
    console.log("fetch Theatre saga called");
    const response = yield call(Api.fetchtheatre);
    yield put(fetchTheatreSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const theatreCRUD = [
  takeLatest(FETCH_THEATRE, fetchTheatre),
  takeLatest(ADD_NEW_THEATRE, createTheatre),
  takeLatest(UPDATE_THEATRE, updateTheatre),
  takeLatest(DELETE_THEATRE, deleteTheatre)
];
