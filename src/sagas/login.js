import { put, call, takeLatest } from "redux-saga/effects";
import { USER_LOGIN } from "../actions/actionConstants";
import Api from "../api";
import { loginSuccess } from "../actions/login";

export function* login(value) {
  try {
    console.log("sagas -> Login : called");
    const response = yield call(Api.login, value.data);
    console.log("sagas -> Login Response : ", response);
    yield put(loginSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const loginSagas = [takeLatest(USER_LOGIN, login)];
