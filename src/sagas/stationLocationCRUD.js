import { put, call, takeLatest } from "redux-saga/effects";
import Api from "../api/stationLocationCRUD";
import {
  fetchStationLocationSuccess,
  deleteStationLocationSuccess,
  updateStationLocationSuccess,
  createStationLocationSuccess
} from "../actions/admin";
import {
  ADD_NEW_STATIONLOCATION,
  FETCH_STATIONLOCATION,
  UPDATE_STATIONLOCATION,
  DELETE_STATIONLOCATION
} from "../actions/actionConstants";

// StationLocation
export function* createStationLocation(value) {
  try {
    console.log("Create StationLocation Saga Called");
    const response = yield call(Api.createstationlocation, value);
    yield put(createStationLocationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateStationLocation(value) {
  try {
    console.log("update StationLocation Saga Called");
    const response = yield call(Api.updatestationlocation, value);
    yield put(updateStationLocationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteStationLocation(value) {
  try {
    console.log("delete StationLocation Saga Called");
    yield call(Api.deletestationlocation, value);
    yield put(deleteStationLocationSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchStationLocation() {
  try {
    console.log("fetch StationLocation saga called");
    const response = yield call(Api.fetchstationlocation);
    yield put(fetchStationLocationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const stationLocationCRUD = [
  takeLatest(ADD_NEW_STATIONLOCATION, createStationLocation),
  takeLatest(FETCH_STATIONLOCATION, fetchStationLocation),
  takeLatest(UPDATE_STATIONLOCATION, updateStationLocation),
  takeLatest(DELETE_STATIONLOCATION, deleteStationLocation)
];
