import { put, call, takeLatest } from "redux-saga/effects";
import {
  ADD_NEW_CALCULATION,
  FETCH_DISTRIBUTION,
  ADD_NEW_DISTRIBUTION,
  UPDATE_DISTRIBUTION,
  DELETE_DISTRIBUTION,
  FETCH_SUBDISTRIBUTION,
  ADD_NEW_SUBDISTRIBUTION,
  UPDATE_SUBDISTRIBUTION,
  DELETE_SUBDISTRIBUTION,
  FETCH_CALCULATION,
  UPDATE_CALCULATION,
  DELETE_CALCULATION
} from "../actions/actionConstants";
import Api from "../api";
import {
  fetchCalculationSuccess,
  deleteCalculationSuccess,
  updateCalculationSuccess,
  createCalculationSuccess,
  fetchSubDistributionSuccess,
  deleteSubDistributionSuccess,
  updateSubDistributionSuccess,
  createSubDistributionSuccess,
  fetchDistributionSuccess,
  deleteDistributionSuccess,
  updateDistributionSuccess,
  createDistributionSuccess
} from "../actions/admin";

// Distribution -------
export function* createDistribution(value) {
  try {
    console.log("Create Distribution Saga Called");
    const response = yield call(Api.createdistribution, value);
    yield put(createDistributionSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateDistribution(value) {
  try {
    console.log("update Distribution Saga Called");
    const response = yield call(Api.updatedistribution, value);
    yield put(updateDistributionSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteDistribution(value) {
  try {
    console.log("delete Distribution Saga Called");
    yield call(Api.deletedistribution, value);
    yield put(deleteDistributionSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchDistribution() {
  try {
    console.log("Fetch Distribution  Saga Called");
    const response = yield call(Api.fetchdistribution);
    yield put(fetchDistributionSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

// SubDistribution ----------
export function* createSubDistribution(value) {
  try {
    console.log("Create Sub Distribution  Saga Called");
    const response = yield call(Api.createsubdistribution, value);
    yield put(createSubDistributionSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateSubDistribution(value) {
  try {
    console.log("update SubDistribution Saga Called");
    const response = yield call(Api.updatesubdistribution, value);
    yield put(updateSubDistributionSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteSubDistribution(value) {
  try {
    console.log("delete SubDistribution Saga Called");
    yield call(Api.deletesubdistribution, value);
    yield put(deleteSubDistributionSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchSubDistribution() {
  try {
    console.log("Fetch Sub  Distribution  Saga Called");
    const response = yield call(Api.fetchsubdistribution);
    yield put(fetchSubDistributionSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

// Calculation
export function* createCalculation(value) {
  try {
    console.log("Create Calculation Saga Called");
    const response = yield call(Api.createcalculation, value);
    yield put(createCalculationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* updateCalculation(value) {
  try {
    console.log("update Calculation Saga Called");
    const response = yield call(Api.updatecalculation, value);
    yield put(updateCalculationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}
export function* deleteCalculation(value) {
  try {
    console.log("delete Calculation Saga Called");
    yield call(Api.deletecalculation, value);
    yield put(deleteCalculationSuccess(value));
  } catch (error) {
    console.log(error);
  }
}
export function* fetchCalculation() {
  try {
    console.log("fetch Calculation saga called");
    const response = yield call(Api.fetchcalculation);
    yield put(fetchCalculationSuccess(response.data));
  } catch (error) {
    console.log(error);
  }
}

export const adminSagas = [
  takeLatest(FETCH_DISTRIBUTION, fetchDistribution),
  takeLatest(ADD_NEW_DISTRIBUTION, createDistribution),
  takeLatest(UPDATE_DISTRIBUTION, updateDistribution),
  takeLatest(DELETE_DISTRIBUTION, deleteDistribution),

  takeLatest(FETCH_SUBDISTRIBUTION, fetchSubDistribution),
  takeLatest(ADD_NEW_SUBDISTRIBUTION, createSubDistribution),
  takeLatest(UPDATE_SUBDISTRIBUTION, updateSubDistribution),
  takeLatest(DELETE_SUBDISTRIBUTION, deleteSubDistribution),

  takeLatest(ADD_NEW_CALCULATION, createCalculation),
  takeLatest(FETCH_CALCULATION, fetchCalculation),
  takeLatest(UPDATE_CALCULATION, updateCalculation),
  takeLatest(DELETE_CALCULATION, deleteCalculation)
];
